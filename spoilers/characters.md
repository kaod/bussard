# Governments

This will make more sense if you read history.md first.

## Solar Union

Home of humanity, but regressed from its height. Suffered from having
portal delayed due to accident. Some believe the Defiant was sabotaged
on its mission to carry a portal to Sol.

* [x] Earth
* [x] Mars
* [x] Newton Station
* [x] Nee Soon Station

## Terran Republic

Most powerful, but only controls two systems, Lalande and
Ross. Formerly included Sol. Regulates portal technology.

* [x] Kala Lamar (Lalande)
* [x] Pinan (Lalande)
* [x] Kuchang Station (Ross)

## Principality of Darush

Independent station in a Terran system (Ross). Grandfathered into
independence early on. Isolated, wealthy, and small.

* [x] Darush (Ross)

## Kingdom of Bohk

Due to a lack of communication, developed fairly independently from
the Terran Republic. Has two internal portals under oversight from
Terrans. Helped Lalande re-establish contact with Katilay, and also
helped Yueh establish its Delta Pavonis colony.

* [ ] Bohk Prime
* [ ] Warnabu Station
* [ ] Banga
* [ ] Galure (New Phobos)
* [ ] Tirakir (Mecalle)

## Yueh

Source of where the portal tech was discovered. Runs two portals (Bohk
and Delta Pavonis) and has two portals (Kowlu and Wolf 1481) run by
Lalande, which is a point of tension.

* [ ] Yueh Prime
* [ ] Da Kau Station
* [ ] Bata Beng (Kowlu)
* [ ] Sim Roen (Kowlu)
* [ ] Packsi (Delta Pavonis)
* [ ] Steele Station (Wolf 1481)

The portal to Delta Pavonis is secretly a multiportal; it can take you
to a few other locations if you have proper access.

## Republic of Katilay

Conflict-wracked even within a single system. Portal was faulty upon
arrival; contact with Lalande only recently re-established.

* [ ] Katilay Prime
* [ ] Slork (station)

## Tana Protectorates

Newest government, close ties still to Solar Union. Rich in
minerals. Terrans allow Sol to operate portals keeping Tana running,
but Tana is completely dependent upon Sol for food and Terrans for
transport, leaving it quite vulnerable.

* [x] Tana Prime (Tana)
* [x] Lioboro (Tana)
* [ ] Kenapa Station (Tana)
* [x] Solotogo (Wolf 294)
* [x] Apkabar Station (Luyten)
* [x] Mirduka Station (L 668-21)

Tana has the most worlds, but the smallest population.

## Human Worlds League

Originally the Terran Republic covered all inhabited worlds, but as
new colonies sprung up independently or declared independence, Bohk
and Yueh spearheaded the League soon after the war. Terran Republic
was also a founding member. Sol joined upon its independence, Katilay
joined several years after re-establishing connection. They oversee
trade agreements, enact spaceflight safety regulations, administrate a
unified currency, coordinate exploration efforts, and work to protect
peace. Nominally they aim to advance shared scientific progress, but
little of this happens in practice. The league is headquartered on
Bohk Prime.

(TODO: Find a way to fit in significance of Darush, New Phobos, Kowlu)

# Companies

* Ceres Shipyards (Sol)
  Responsible for all early long-haul colony ships and all Tana
  ones. All long-haul cargo ships made here. Located in the asteroid
  belt.

* Consolidated Shipping (Ross)
  Runs most inner-system routes. Starting to break into Tana lines,
  but only slowly.

* Songket Shipyards (Lalande)
  Responsible for most later colony ships, but also creates many
  interplanetary cargo ships.

* Kosaga Shipyards (small, Bohk)
  Started in order to create the New Phobos colony ship, but now only
  focuses on interplanetary ships. Created two colony ships for Yueh.

* Ares Mineral Company (Lalande)
  Dominant mining company.

* Allied Deliveries (Yueh)
  Handles most eastern delivery routes.

* Aperture Technology (Lalande)
  Caretaker of portal artifacts, researches and maintains portal
  technology. Closely guards what little they know of how they work.

* Interstellar Communication Systems (Lalande)
  Works with Aperture to piggy-back data transmission on top of each
  portal open cycle.

* Luminous Enterprises (Yueh)
  Originally adapted portal technology for human construction. Barred
  from further portal development by treaty, but continuing it
  covertly. Can't colonize traditionally without constructing large
  colony ships, which would be noticed, so they are researching
  coldsleep as an alternative.

* Post-Terran Mining Company (Tana)
  The primary driver behind the colonization of Tana. Government of
  Tana is arguably a front for PTMC.

* Orolo Research (Sol)
  Responsible for early sublight drives, recently assisting Luminous
  with coldsleep research.

* GNO Project (Sol)
  Responsible for maintaining software infrastrucutre such as the Orb OS.

* TMRC (Bohk, Yueh)
  Loose association of hackers, mostly interested in cool onboard computer
  tricks, but also with some interest in security.

# Universities

...

# Spaceships

## Early Interplanetary ships

## Colonizers

Self-sufficient ships with crews numbering in the mid-hundreds. Upon
arrival the ships are disassembled and the parts used to construct
either planetary colonies or re-appropriated into orbital stations.

* First Wave: SS Lorentz (Lalande, 0.3c), SS Copernicus (Ross, 0.5c)
* Second Wave: SS Bradbury (Bohk, 0.6c),
  SS Eratosthenes † (New Phobos, 0.5c), SS Defiant (Yueh, 0.6c)
* Third Wave: SS Kepler † (Kowlu)
* Fourth Wave: SS Las Casas (Katilay), SS Cherenkov † (Mecalle),
  SS Sagan (Delta Pavonis)
* Fifth Wave: SS Pythagoras (Luyten), SS Planck (Tana),
  SS Rosen (Wolf), SS Archimedes (L 668-21)

† - ship from Bohk

From third-wave on they all have portals onboard and fly at 0.7c.

## Portal ships

Newer colonizers have portals on them, but existing colonies need
ships containing just the equipment needed to create the portal.

* Original portal ship: SS Defiant (converted from Yueh colonization)
 * Yueh->Lalande
 * Lalande->Sol (accident)
* Second portal ship: SS Calandria
 * Lalande->Ross
 * Bohk->New Phobos
 * Mecalle->Katilay
* Defiant Rescue ship: SS Goddard
* Bohk trade ship: SS Oberth
 * carries portal from Yueh back to Bohk

## Modern Interplanetary ships

Engines capable of lots of maneuvering around a star but not long-term
flight. They need significant battery if they are to activate a
portal. Mostly they carry cargo and passengers; occasionally
scientific equipment.

## Non-colonizing exploration ships? (automated?)

# Historical Characters

* Captain Darush, saved the SS Copernicus on the trip to Ross
* Dr. Soong, credited (miscredited?) with discovery of portals, founded Aperture
* Captain Jarkad of SS Goddard
* Dr. Jameson, Yueh Researcher who replicated portal tech
* Captain Armiger of the original Defiant mission
* Captain Irons of Lalande bomber squad from the war
* Captain A.S. of SS Pythagoras killed in coup
* General who took over afterward?
* Opposing general?

# Contemporary Characters

* Tana
 * PTMC chairman (very old)
 * Mining captain, served on SS Archimedes
 * Kosaga Shipyards engineer
 * University Students
   * Ricarad Jinod: developing more advanced sensor array
* Sol
 * Garibaldi: Orolo researcher, eventually discovers domain injector
* Yueh
 * Luminous engineers (2 or 3?)
 * University students
* Bohk
 * Cargo captain
