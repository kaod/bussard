# Welcome to Bussard!

If you're just getting started, you'll want to acquaint yourself with
the flight controls first. Right now you're reading this in console mode,
which is a console in which you can interact with your ship's onboard
computer. You'll want to hit ESC to get back to flight mode, and ~
will get you back here.

Use the arrow keys to fly around a bit, but keep an eye on the red
fuel gauge in the upper left. The faster you're going, the more fuel
(and time) it will take to slow back down. Your fuel will recharge,
but it takes time. You can't collide with anything, so don't fear.

The striped blue line plots your estimated trajectory, and the box in
the upper right shows your velocity vector. Use the equals and minus
keys to zoom in and out.

Once you've got a feel for the controls and instrumentation, run this
in the console to continue:

    man("intro2")
