return {
   name="Deliver sensor to Mars for Jinod",
   id="c1168903-c17e-4082-a9f1-7662b1f51c7a",
   destinations={"Mars"},
   success_events={"jinod1"},
   credits=300,
   cargo={["sensor_array"]=2},
   success_message="Thanks for delivering my sensors!",
}
