-- -*- lua -*-
return {
   name="Med-evac to Earth",
   description="Head to Earth as quickly as possible.",
   id="1b1763f7-d79d-4322-9638-b9c296a47cc0",
   destinations={"Earth"},
   success_events={"medevac"},
   credits=400,
   time_limit=10000,
   fail_message="Looks like it's too late for this med-evac.",
   success_message="Thanks for getting us back to Earth!",
}
