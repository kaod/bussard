# Lonesome Planet Map

The Lonesome Planet Map contains a survey of all the known colonized
systems in human space. Portal connections are clearly marked,
distances are to scale, and color-coding is used to show the
affiliations of each system at the time of publication:

* White: Solar Union
* Green: Terran Republic
* Yellow: Kingdom of Bohk
* Red: Yueh
* Purple: Republic of Katilay
* Turquoise: Tana Protectorates

Happy Travels!

Copyright © 2430 Lonesome Planet Publishing, All Rights Reserved.
