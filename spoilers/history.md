# Bussard Historical Background

## Early colonization Timeline

The establishment of the Terran Republic.

* 2038 - Mars colonized

* 2049 - Newton Station operational, becomes stop-off point for belt miners

* 2099 - First interstellar colony ship (SS Lorentz) launches to
  Lalande (1.7 pc @ 0.3 c, 18y)

* 2110 - Nee Soon Station operational, outer reaches of Sol system

* 2117 - SS Lorentz arrives, colonizes Kala Lamar in Lalande

* 2128 - SS Copernicus colony ship launches from Sol to Ross
  (1.6 pc @ 0.5 c, 10y, γ=1.2)

* 2138 - SS Copernicus arrives, colonizes Ross

* 2143 - Errant cargo shuttle threatens Ross colony; Captain Darush
  saves the colony by colliding his ship into the shuttle to push it
  off course, leading to stricter safety regulations

* 2153 - Darush is granted independence due to heroism of
  captain; station in Ross system remains under Terran control

* ... 20 years: Slow trade routes between Sol, Lalande, Ross grow

## Second Wave of slow colonization

Ross rises to prominence through its academic institutions, including
Kuchang Station, which is well-situated to study many phenomena. Most
slow-colonized worlds tend to lack close ties with their colonizing
power, but Ross and Lalande grow close. Lalande's great mineral
deposits lead to it becoming an industrial powerhouse.

* 2175 - SS Bradbury launched from Lalande to Bohk (2 pc @ 0.6 c, 10.8y, γ=1.3)

* 2187 - SS Bradbury colony ship arrives in Bohk, establishes
  independent colony

* 2194 - News of Bohk's independence reaches Lalande

* 2200 - News of Bohk's independence reaches Sol

* 2212 - Bohk launches SS Eratosthenes to New Phobos (1 pc, 0.5 c, 6.5y)

* 2218 - SS Eratosthenes arrives, colonizes New Phobos, maintains
  close ties with Bohk

* 2254 - Lalande launches SS Defiant to Yueh (2.9 pc, 0.6 c, 15.7y)

* 2269 - Yueh colonized by SS Defiant, portal technology discovered,
  pressure on colonists not to declare independence

* 2272 - SS Defiant departs to bring portal tech to Lalande
  (2.9 pc, 0.6c, 15.7y)

Dr. Jameson, chief scientists on board the Defiant, is actually from
Bohk originally. He wants Yueh to be independent the same way Bohk
was. The remains of the portal research lab are discovered (by Jameson
or Capt. Armiger; it's historically unclear); Armiger insists the
findings are property of the military, but Jameson wants to keep it to
the colonists. Jameson ends up killing Armiger to prevent it falling
into what he thinks of as the wrong hands.

## Third Wave: portals

The initial discovery of the portals had enough parts for three
portals to be constructed. The Defiant returns to Lalande not because
they want Lalande to gain access to the portals, but because they are
worried their colony will not survive without supplies from a more
established civilization.

* 2287 - SS Defiant returns to Lalande

* 2291 - First portal between Yueh and Lalande operational

* 2293 - SS Defiant departs to bring portal to Sol (1.7 pc, 0.6c, ...)

* 2294 - SS Calandria departs Lalande for Ross with portal
  (3 pc, 0.7 c, 14y, γ=1.4)

* 2296 - SS Defiant is knocked off course; engines damaged (1.2 pc away)

* 2300 - SS Defiant distress signal received; SS Goddard launched to rescue

* 2308 - SS Calandria arrives at Ross, portal becomes operational

* Lalande, Ross, Yueh grow powerful thru portal trade, Sol's power wanes

## War!

After the war, the story is that Lalande had found and studied the
portals from the beginning. Everyone on Yueh knows that it was those
from Yueh (Jameson) that actually discovered how to bring them online.

* 2310 - SS Goddard reaches stranded SS Defiant

* 2311 - Yueh scientists independently replicate portal tech

* 2312 - Yueh returns cargo ship SS Oberth to Bohk with portal tech
  (1.2 pc, 0.7c, 5.5y, γ=1.4), launches colonizer SS Kepler to Kowlu
  (2.4 pc, 0.7c, 11y)

* 2313 - Lalande attacks Yueh, dropping asteroids on them from orbit

* 2314 - Peace between Yueh and Lalande, Yueh becomes independent, but
  under the agreement that Lalande controls all existing portals
  (except Yueh/Bohk and Yueh/Kowlu) and retains a monopoly on
  newly-constructed ones

* 2317 - Bohk, Yueh, and Terrans found Human Worlds League in an
  uneasy alliance

* 2318 - SS Oberth arrives in Bohk, first human-constructed portal
  (to Yueh) operational

* 2320 - SS Goddard returns to Sol, Lalande/Sol portal operational
  but Sol secedes from Terran Republic when they find about the war

## Fourth Wave: constructing portals and Katilay crisis

* 2322 - Bohk portal to Lalande established, first portal constructed
  without portal ship

* 2323 - Portal between Ross and Sol operational; also constructed
  without portal ship

* 2323 - SS Kepler arrives in Kowlu, colonizes it

* 2325 - Metaphysics researchers at University of Darush discover
  evidence of another multiverse

* 2330 - Lalande launches SS Pythagoras to Katilay (4 pc, 0.7c, 18y, 12.8ly)

* 2333 - SS Calandria departs for New Phobos with portal (1 pc, 0.7 c, 4.6y)

* 2338 - New Phobos portal operational under Lalande supervision

* 2344 - Bohk luanches SS Cherenkov colonizer to Mecalle
  (0.8 pc, 0.7 c, 3.7y)

* 2345 - Metaverse traversal discovered to only be possible for
  machine consciousness; research begins

* 2348 - SS Pythagoras arrives in Katilay and colonizes, but its portal
  proves unstable, coup enacted

* 2351 - SS Cherenkov establishes colony in Mecalle

* 2359 - Traxus exhibits early signs of rampancy

* 2360 - Lalande learns of Pythagoras's fate

* 2362 - Lalande launches SS Calandria from Mecalle to Katilay (2.4 pc, 0.7c, 11y)

* 2366 - Lalande launches SS Sagan colonizer from Kowlu to Delta Pavonis
  (0.7 pc, 0.7c, 3.2y)

* 2367 - Traxus crashes a ship and escapes into the Yueh network

* 2368 - Machine Consciousness Research and Operation Ban passes

* 2368 - Colonists from Kala Lamar found Pinan colony in Lalande

* 2369 - Lalande/Sol relations improve somewhat

* 2370 - SS Sagan arrives in Delta Pavonis, colony established

* 2373 - SS Calandria arrives in Katilay, portal to Mecalle
  established, Lalande portal fixed

## Fifth wave: Tana's colonization

* 2374 - Lalande agrees to allow Sol to colonize Tana

* 2378 - Sol launches colony ships to 4 Tana systems: SS Tyson
  (Luyten), SS Planck (Tana), SS Poincare (L 668-21), and SS Rosen
  (Wolf 294)

* 2380 - Bohk AC researcher team's work is uncovered, they move to Katilay

* 2385 - Katilay finally joins League

* 2386 - Player's ship is launched from Katilay (9.9 parsecs, 0.7c, 45y)

* 2396 - SS Tyson arrives in Luyten, colonizes (3.8 pc, 0.7c, 17.7y)

* 2402 - SS Rosen arrives in Wolf 294, colonizes (5.3 pc, 0.7c, 24.7y),
  SS Planck arrives in Tana, colonizes (5.3 pc, 0.7c, 24.7y)

* 2410 - It becomes clear that something happened to the SS Poincare;
  it is given up for lost

* 2412 - Colonists from Tana Prime colonize Lioboro.

* 2418 - Tana launches SS Archimedes colony ship to L 668-21 (2.1
  pc, 0.7c, 9.8y)

* 2427 - SS Archimedes colony ship arrives in L 668-21, establishes station colony

* 2431 - Player's ship arrives at L 668-21, game begins.
