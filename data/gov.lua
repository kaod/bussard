return { treaties = {["Sol"] = {["Tana"] = true},
            ["Tana"] = {["Sol"] = true},},
         adjacent = {
            ["Tana"] = {["Sol"] = true},
            ["Sol"] = {["Terran"] = true, ["Tana"] = true},
            ["Terran"] = {["Sol"] = true, ["Katilay"] = true,
               ["Bohk"] = true, ["Yueh"] = true},
            ["Katilay"] = {["Terran"] = true, ["Bohk"] = true},
            ["Bohk"] = {["Terran"] = true, ["Katilay"] = true, ["Yueh"] = true},
            ["Yueh"] =  {["Terran"] = true, ["Bohk"] = true},
         },
}
