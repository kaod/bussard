# Bussard changelog: history of user-visible changes

## beta-1 / ???

* Rename repl to console.
* Automatically adjust trajectory settings to achieve decent frame rate.
* Change default quit key to ctrl-q.
* Save state of asteroids and ships.
* AI-controlled ships.
* Persist console buffer and command history.
* New `clear()` function (clears the console buffer).

## alpha-6 / 2015-12-24

* A few more missions.
* Tab completion (only for the in-ship repl).
* Add a map upgrade.
* (Most) worlds tell you a bit about them when you log in.
* Editor can undo/redo.
* Added Comm Boost and Solar Panel upgrades.
* Finish adding the rest of the worlds.
* Offer to save the game in case it crashes.
* Add lisp OS which runs on portals and some stations.
* Allow reading manual pages before buying.
* Add ability to sell upgrades.

## alpha-5 / 2015-11-16

* Add limited support for glob patterns in shell.
* Create fuel charge booster upgrade.
* Performance increases.
* Add mission system and a handful of missions.
* Fix a bug where upgrades would not be available.
* Populated systems for Katilay and Bohk.
* Implemented clearance system for travel between governments.
* Portals now function by their own onboard computers too.
* Fix a bug where multibyte input would crash the game.

## alpha-4 / 2015-10-29

* Show readable table return values in repl.
* Change some key bindings invoking for repl and editor.
* Add tutorial.
* Create news system.
* Add in-game independent clock.
* Add portal animation.
* Fix a bug where flying would scramble repl input.
* Add power mechanics.
* Fix kill ring commands.
* Add API for in-game functions to run periodically.
* Add mark ring and pop-mark command to editor.
* Add Sol system.
* Change trajectory plot indicators to striped lines.
* Require portals be activated before traveling through them.
* Move HUD definitions to config file; allow reconfiguration.

## alpha-3 / 2015-09-27

* Boost range at which you can log in to stations.
* Fix a bug where files in station OSes couldn't load from releases.
* Other minor bugfixes.

## alpha-2 / 2015-09-25

* Rudimentary text editor for onboard config.
* Add onboard help system and some manual pages.
* Refuel at stations.
* Station filesystems get saved/restored upon restart.
* Enable buying non-guest accounts on station computers.
* Allow copying of files to/from station accounts.
* Customizable keyboard commands.
* Upgrades available for purchasing from station computers.
* Increased ship mass makes acceleration more sluggish.

## alpha-1 / 2015-09-15

* Initial alpha functionality.
* Flight works with realistic gravity/thrust mechanics.
* Portals to jump between systems.
* Onboard REPL with sandboxing.
* Logging into space station computers with faux-unix.
* Asteroid mining using lasers.
* Trade cargo on stations.
