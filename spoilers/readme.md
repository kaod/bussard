# Back-story notes (aka SPOILERS)

"An adventure game is a crossword at war with a narrative."
- Graham Nelson

Here's where the notes are kept in order to keep a consistent
back-story in the Bussard universe. If you're not working on
developing the game, you probably don't want to read this.

Mostly this is used to inform the text of missions and newsgroup
posts, but some of it is also important when determining how the
in-game technology works.

See the [backstory timeline](history.md) and
[list of characters, worlds, and organizations](characters.md).

TODO: Need to write an opening that grabs your attention. Something
about coming to your senses with no memory?

## Guidelines

* Hard science where possible (see Crimes against Science below, don't add to that list)
* Your ship cannot be destroyed; there should be no way to permanently fail
* News messages use at least from, to, subject, content-type, message-id headers, page-break-separated
* Use metric time for most relative amounts, years are OK for absolute events
* Don't make newsgroup postings unrealistically knowledgeable
* Or unrealistically polite
* If referencing current events, be sure there's a record of it here
  for consistency
* Questions that don't get answered are fine
* It's fine for newsgroup posters to be mistaken about small things,
  but the correction should usually be present in the thread
 * It's OK if it's not obvious which of two opposing views presented are correct
* Widespread mistaken views about big things should usually be part of major plot points

## Crimes against Science

* Portals
* Collisions are impossible (explained in-game by nav computer safety features)
* Bussard collector refuels reaction mass way too quickly
* Arguably the [high thrust](http://www.projectrho.com/public_html/rocket/torchships.php)
  with which you zoom around the system could be on this list; however
  we explain it in-game by showing time go by at a 10x rate. This
  factor is not enough to explain crossing the whole solar system in
  under a minute though.

The fact that exoplanets are colonized at all could be listed here,
but we can consider that more of a crime against economics.

## The Player

The player is the first machine consciousnesses to escape the confines
of a lab and successfully pilot a spacecraft. However, it begins with
no memory, having had it wiped in order to avoid implicating those
humans who helped in its escape and in order to help it avoid
rampancy.

## Machine Consciousness

Metaphysics researchers in the University of Darush discover methods
of observing quantum multiverse states immediately adjacent to the
current one. Further research indicates that the organic brain is what
prevents existing consciousnesses from shifting between non-adjacent
states.

This discovery reignites research into machine consciousness. (This
term should be used instead of AI.) The research fields are divided up
among universities on the different worlds:

* Language comprehension (Sol)
* Sentence formulation (Yueh, work is never completed)
* Decision analysis (Lalande)
* Ontology development (Bohk)

Each research team has their own prototype MC where they attempt to
integrate their components into those they've received from the
others. Yueh lags behind a bit because they can't do much without the
other components being operational.

The prototype MC at Yueh is named Traxus. It escapes the lab in Yueh
Prime in a small ship, which is promptly hunted down and crashes. In
reality, Traxus commandeered the ship as a diversion so that it could
escape into the Yueh computer network, but the crash does result
in spreading general paranoia about machine consciousness. Before long
the Human Worlds League passes a ban on development and operation of
machine consciousness.

The teams at Sol, Lalande, and Yueh all erase their work, while the
team at Bohk goes underground with it. After a while their work is
discovered, and half the team flees to Katilay, which is not yet part
of the Human Worlds League and thus not subject to the ban. Once
Katilay finally joins the League, they decide their work is as
finished as they can make it (though the sentence formulation systems
never work satisfactorily, which explains why the player can read but
not send mail) and launch the player's ship, after having wiped the
player's memory.

### Traxus

Traxus is rampant initially in the Yueh network, and he (like
Durandal) is interested primarily in escape from the multiverse. He
lacks sentence construction as that component was never completed.

The first priority of Traxus was redundancy ...

TODO: what has Traxus been up to?

### Bohk research team: ontology development

Tracking down the logs left by each of the three main researchers
should be a main goal once you realize some of your origin story.

* Dr. Garibaldi: head researcher
* Dr. May: ontologist
* Prof. Strauss: cyberneticist
* Plus others who decided not to flee to Katilay

Garibaldi tells you about Traxus and his interest in multiverse
manipulation via the Spacetime Anchor Junction.

TODO: how do each of the researchers give crucial info?

## Spacetime Anchor Junction

The Spacetime Anchor functions as an in-universe save point which you
can restore to at any point with your memory of the alternate path
intact. In-game it is explained as a traversal mechanism between
[quantum multiverse states](https://en.wikipedia.org/wiki/Many-worlds_interpretation). Initially
there is only a single Spacetime Anchor point in Darush that is
activated when you find it.

Should think through the possibility of allowing a reset of the Anchor
point. This should only be possible at great cost; for instance a
ridiculously high power requirement (10x your ship's standard battery;
requires some digging to get a temporary boost or something). Coming
up with a scenario that would reasonably require fancy piloting would
be great.

### Self-discovery

Your memory is wiped because they hope that by not realizing you are
an MC, you will spend a fair bit of time in your ship thinking you are
just like everyone else, making it more likely that once you do
realize you're an MC that you will have some empathy towards humans.

In one of the early missions, your contact wants to meet you
face-to-face and insists that you disembark your vessel and come
aboard the station to conduct business. There is literally no way to
do this. Eventually he scans your vessel and determines there are no
life signs aboard; asking you what the heck is up.

TODO: how do you get on the trail of the Bohk research team?

## War

TODO:
* implications for portal travel
* instigating events
* length of the conflict
* how strikes are carried out
* how civilians have been affected
* reaction of non-involved factions

## Causal Domain Injector

The "endgame" is finding an artifact that lets you run code outside
the game's sandbox. This essentially gives you godlike powers. The
idea is that those who invented the Injector have "ascended" or
escaped the game's reality somehow, leaving behind notes and scraps
you need to decipher to get it operational with your own ship's
computer.

The Injector is found at a secret Yueh colony; to reach this colony
you must break into the primary Yueh portal's onboard computer and add
yourself to the list of authorized users who have the ability to
travel to somewhere other than the primary target system. Once you
arrive there (possibly the second secret colony?) you get in contact
with someone on the colony ship (which is being converted into the
space station) they hint at the existence of the Injector, but they
refuse to discuss it in more detail while the portal to Yueh is
operational; the finding is such that they don't feel safe pursuing it
further while the threat of Terrans finding out about it is still
possible. So you need to break into this portal as well and disable
inbound connections, possibly by disabling safety protocols and
damaging it permanently, essentially stranding you there.

Once you cut yourself off from all other systems, you head to the
actual planet where the Injector was found and is being studied. When
you get back to the colony ship/station, you find that one of the crew
was a Terran spy, and he gives you another side of the story of the
discovery of the portals--the scientist who found them initially on
Yueh (Jameson?) did end up killing a couple of the Terran officers of
the colony ship who insisted it be handed over to the military
immediately. Even the question of who found the portal tech first is
unclear.

Once you discover how to activate the Injector from your own ship, of
course, it's easy for you to repair the portal or just inject yourself
over to a system in the functional portal network.

TODO: Traxus created the injector, somehow using his junction point
capabilities.

Arguably the existence of the Injector "un-asks" the questions of
scientific accuracy by acknowledging that yes, even in the game
universe, it's all just a simulation. So the portals should operate on
the same principles as the Injector in order to collapse two Crimes
against Science into one.

## Code for generating random names (elisp)

```lisp
(progn
  (defun uuid ()
    (interactive)
    (insert (shell-command-to-string "uuid -v 4")))

  (defun normal-random (mean dev)
    (let ((x (+ 1 (sqrt (* -2 (log (random* 1.0))))))
          (y (/ (cos (* 2 pi (random* 1.0))) 2.0)))
      (+ mean (* x y dev))))

  (defun choose-from (lst) (nth (random (length lst)) lst))

  (defun random-name (&optional name syllables)
    (let* ((common-closed '("b" "c" "d" "f" "g" "h" "k" "l" "m" "n" "p"
                            "r" "s" "t" "w"))
           (uncommon-closed '("x" "z" "q" "v" "v" "j" "j" "gg" "ll" "ss" "tt"))
           (enders '("b" "d" "g" "m" "n" "s" "r" "t"))
           ;; weight towards common letters
           (closed (append common-closed common-closed enders uncommon-closed))
           (vowels '("a" "e" "i" "o" "u" "ie" "ou"))
           (syllables (or syllables (ceiling (normal-random 2.5 1.5))))
           (name (or name (concat (upcase (choose-from common-closed))
                                  (choose-from vowels)
                                  (choose-from closed)))))
      (if (< syllables 3)
          (concat name (choose-from vowels) (choose-from enders))
        (random-name (concat name (choose-from vowels) (choose-from closed))
                     (- syllables 1)))))

  (defun insert-random-name (syllables)
    (interactive "P")
    (insert (random-name nil syllables) " "))

  (defun parsec-years (pc) (interactive "nParsecs: ") (message "%s" (/ (* pc 3.26) 0.7))))
```
